
[![Netlify Status](https://api.netlify.com/api/v1/badges/00b98b81-f2ab-49b3-b966-405745867691/deploy-status)](https://app.netlify.com/sites/kind-hamilton-1c612a/deploys)

## Requerimientos

Instalar desde el codigo:  

```bash 
sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt update
sudo apt install golang-go
go install github.com/gohugoio/hugo@latest
```

Instalacion usando Snap:

```bash 
sudo snap install hugo
```


## Guia de uso


Para iniciar el server: 

```bash
make start
```
