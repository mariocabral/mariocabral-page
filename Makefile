.PHONY: ubuntu-setup


ubuntu-setup:
	sudo apt-get update
	sudo apt install hugo

start:
	hugo server -w
